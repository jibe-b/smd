s_\*_\n\n*_g
s_!\ *\(\[[^]]*\]\)\ *\(([^.]*[^ ]\)\ *\(\.\)\ *\(png\)\ *)_!\1\2\3\4)_g


s_\$\ *[Ss]_@Thing §"rdf:typeOf" _g

s_\$\ *N_[] §sof:note _g
s_\$\ *L_ §rdfs:label _g
s_\$\ *D_${DATE}_g

#s_\$_*X_ §x:X _g
#s_\#_\n&_g


#Possibly substitute caracters with their unicode version
#http://www.utf8-chartable.de/
#s_'_U0027 _g
s_[éèê]_e_g
s_[ÉÈÊË]_E_g
s_[àä]_a_g
s_[ÀÄ]_A_g
s_[ôö]_o_g
s_[ÔÖ]_O_g
s_ï_i_g
s_Ï_I_g
s_[ûü]_u_g
s_[ÛÜ]_U_g


#Possibly substitute caracters with their unicode version
#http://www.utf8-chartable.de/
#s_'_U0027 _g
#s_é_U00E9 _g
#s_É_U00C9 _g
#s_è_U00E8 _g
#s_È_U00C8 _g
#s_à_U00E0 _g
#s_À_U00C0 _g
#s_ê_U00EA _g
#s_Ê_U00CA _g
#s_ô_U00F4 _g
#s_Ô_U00D4 _g
#s__ _g





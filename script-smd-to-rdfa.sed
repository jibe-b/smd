#
        ##Long labels
        ###without namespace
#s_\([@§&]\ *\"\)\([^"]*\)\(\ *\"\)_\n\1\2\3">\2</div>\n\#_g
        ###with namespace
#s_\([@§&][^:]*\:\ *\"\)\([^"]*\)\(\ *\"\)_\n\1\2\3>\2</div>\n\#_g
        ##Short labels
        ###with namespace
#s_\([@§&][^:]*\:\ *\)\([^ ^"]\+\)\(\ \)_\n\1\2\3>\2</div>\n\#_g
        ###without namespace
#s_\([@§&]\ *\)\([^ ^"^\:]\+\)\(\ \)_\n\1\2\3">\2</div>\n\#_g







######################################
#Deal with all lines

{:a;N;$!ba;s_\n_//_g;}

#Reopen root graph after graph closing
#s_}_\n&¤graph @g:"Root" {_g

#Transform Md titles into graph openings
#s_\#\+\([^/]*\)//_\n} \ngraph @g:"\1" {_g

#Remove temporary indications of newline
s_//_ _g



#Dealing with [@§]

s_\([@§]\)\ *_\n\1_g

#Dealing with namespace
s_\([@§][^:^ ]\)*\ *\:_\1:_g


#################################################################################

#Tree branching management extension

#with namespace
s_\(¥\)\ *\([^:^ ]*\)\ *\:\ *\(\"[^"]*\"\)_@\2:\3 . @\2:\3_g


################################################################################

#Alternative-label extension
#Concept: the author can write an alternate text that will be kept in the Md document or showed in the html+RDFa document, while the pseudo URI will be used in turtle and RDFa as the real URI

s_\._</div><div>_g

#Dealing with short label after namespace
s_\(@\)\([^ ^:]*\:\)\ *\([^ ^"]\+\)\ *\"\([^"^@^§^%]*\)\"_<div about="\2\3">\4<_g
s_\(§\)\([^ ^:]*\:\)\ *\([^ ^"]\+\)\ *\"\([^"^@^§^%]*\)\"_<span property="\2\3">:Q\4</span>_g
s_\(&\)\([^ ^:]*\:\)\ *\([^ ^"]\+\)\ *\"\([^"^@^§^%]*\)\"_<span content="\2\3">\4</span>_g


#Dealing with long label after namespace
s_\(@\)\([^ ^:]*\:\)\ *\"\ *\([^"]*\)\ *\"\([^"^@^§^%]*\"\)_<div about="\2\3">\4</div>_g
s_\(§\)\([^ ^:]*\:\)\ *\"\ *\([^"]*\)\ *\"\([^"^@^§^%]*\"\)_<span property="\2\3">\4</span>_g
s_\(&\)\([^ ^:]*\:\)\ *\"\ *\([^"]*\)\ "\"\([^"^@^§^%]*\"\)_<span content="\2\3">\4</span>_g


#Dealing with short label without namespace
s_\(@\)\ *\([^ ^:^"]\+\)\(\ *\"[^"^@^§^%]*\"\)_<div about="r:\"\2\"">\3</div>_g
s_\(§\)\ *\([^ ^:^"]\+\)\(\ *\"[^"^@^§^%]*\"\)_<span property="sof:\"\2\"">\3</span>_g
s_\(&\)\ *\([^ ^:^"]\+\)\(\ *\"[^"^@^§^%]*\"\)_<span content="r:\"\2\"">\3</span>_g



#Dealing with long label without namespace
s_\(@\)\(\"[^"]*\"\)\(\ *[^"^@^§^%]*\"\)_<div about="r:\2">\2</div>_g
s_\(§\)\(\"[^"]*\"\)\(\ *[^"^@^§^%]*\"\)_<span property="sof:\2">\2</span>_g
s_\(&\)\(\"[^"]*\"\)\(\ *[^"^@^§^%]*\"\)_<span content="r:\2">\2</span>_g

###############################################################################

#Dealing with short label after namespace
s_\([@§][^ ^:]*\:\)\ *\([^ ^"]\+\)_\1\2">\2</div>_g

#Dealing with long label after namespace
s_\([@§][^ ^:]*\:\)\ *\(\"[^"]*\"\)_\1\2">\2</div>_g

#Dealing with short label without namespace
s_\(@\)\ *\([^ ^:^"]\+\)\(\ \)_\1r:\"\2\"">\2</div>\3_g
s_\(§\)\ *\([^ ^:^"]\+\)\(\ \)_\1sof:\"\2\"">\2</div>\3_g

#Dealing with long label without namespace
s_\(@\)\(\"[^"]*\"\)_\1r:\2">\2</div>_g
s_\(§\)\(\"[^"]*\"\)_\1sof:\2">\2</div>_g

#Removing spaces in labels
{:b;s_\(@[^:]\+\:\"\)\ *\([^ ^"]\+\)\ \([^ ^"]*\)_\1\u\2\u\3_g;tb}
{:c;s_\(§[^:]\+\:\"\)\ *\([^ ^"]\+\)\ \([^ ^"]*\)_\1\2\u\3_g;tc}

#Dealing with literals
s_\(%\)\ *\(\"\)\ *\([^"]\+[^"^ ]\)\ *\(\"\)\ *\(%\)_\n\1\2""\3""\4\5\n\#_g

#Escape punctuation in literals (should be improved to handle multiple punctiation signs)
#{:d;s_\(%\"\"\"[^\.]*\)\.\([^\.]*\"\"\"%\)_\1\$PERIOD\2_g;td}
#{:e;s_\(%\"\"\"[^\;]*\)\;\([^\;]*\"\"\"%\)_\1\$SEMICOLON\2_g;te}
#{:f;s_\(%\"\"\"[^\,]*\)\,\([^\,]*\"\"\"%\)_\1\$COMA\2_g;tf}

#Dealing with punctuation
#s_\([^$]\)\([\.\;\,{}]\)_\1\n\2\n\#_g
#s_\[\ *\]_\n&\n_g
#s_\[_\n&\n\#_g
s_\]_\n&\n\#_g

#Correct the problem of punctuation in literals
#/\"\"\"/,/\"\"\"/{:h;N;$!bh;s_\.\n\#_\._g;th}

#Remove SMd markup
s_[@§]\([^:]\+\:\)\"\ *\([^"]\+\)\ *\"_\1\2_g
s_[@§]\([^:]\+\:\)\([^ ]\+\)\ _\1\2 _g
s_%\"\"\"\([^"]\+\)\"\"\"%_"""\1"""_g
#s_\$\([.,;]\)_\1_g


#Clean begin and end of file
#s_^_\#_g
#s_}__1
#s_$_\n}_g



# useless sudo command to get the root access
echo "Please only give root rights to software you trust. This software is open source and under a free licence. Please consider having a look at the code.
Asking for root rights:"
sudo echo

if [[ ! -d tmp ]]
then
	mkdir tmp
fi

# install cb as a dependency
echo "installing cb, a library based on curl"
sudo cp lib/cb.sh /usr/local/bin/cb



# install dependencies listed in dependency file
echo "installing dependencies"
for pack in $(cat dependencies | sed '{:a;N;$!ba;s_\n_ _}')
do

	a=$(dpkg -s $pack 2> tmp/installed-packages)
	test_if_installed="$(cat tmp/installed-packages | head -n 1 | sed 's_\(.\{10\}\).*_\1_')"
	if [[ "$test_if_installed" == "dpkg-query" ]]
	then
		if [[ "$pack" == "blazegraph" ]]
		then
			echo "preparing dependencies for installing blazegraph"

			sudo add-apt-repository ppa:webupd8team/java <<< "\n"  
			sudo apt update
			sudo apt install oracle-java7-installer
			if [[ ! -f blazegraph.deb ]]
			then
				wget http://downloads.sourceforge.net/project/bigdata/bigdata/2.1.0/blazegraph.deb
			fi
			sudo dpkg -i blazegraph.deb
		else
			echo installing $pack
			b=$(sudo apt-get install  $pack 2> tmp/install-status)
			i_status=$(head -n 1 tmp/install-status | sed 's_\(.\{27\}\).*_\1_')
			
			if [[ "$i_status" == "E: Unable to locate package" ]]
			then
        			echo $pack "was not found in the software repositories. Blazegraph can be installed from http://blazegraph.com"
			fi
		fi
	else 
		echo $pack "was already installed"
	fi

done

erase_existing_data="y"

# possibly erase existing data
if [[ -d  ${GLOBAL_data_directory} ]]
then
	echo "existing data in" ${GLOBAL_data_directory} ". Type  [y] to erase it and install. (default: keep data and abort installation)"
	read erase_existing_data
fi

# install
if [[ $erase_existing_data == "y" ]]
then

	# create data directory if not existing
	if [[ ! -d ${GLOBAL_data_directory} ]]
	then
		mkdir ${GLOBAL_data_directory} ${GLOBAL_data_directory}/tmp
	fi 
	
	# substitute variables in binary file and add to PATH
	cat configuration smd.sh > smd
	sudo mv smd ${GLOBAL_executable_file_path}
        sudo chmod -u+rwx ${GLOBAL_executable_file_path}

	# fill data directory
	rm -rf ${GLOBAL_data_directory}/*
	cp -rf * ${GLOBAL_data_directory}/.  # copy all these files to the data directory

	echo "local data is stored in" ${GLOBAL_data_directory}    
	echo "Now run a RDF graph database and make use of the commands: smd ..."

	else 
	echo "software not installed"

fi







# sudo rm /usr/local/packages/graph-executioner_0.0.1-1_amd64.deb ;sudo checkinstall --pkgname "gexec-blabla" --pkgversion "0.0.1" --requires "curl,wzip"; sudo mv *.deb /usr/local/packages/gexec-blabla_0.0.1-1_amd64.deb;sudo ~/bin/update-mydebs ; sudo apt update ;sudo apt install gexec-blabla


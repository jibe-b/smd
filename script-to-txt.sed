s_!\ *\(\[[^]]*\]\)\ *\(([^.]*[^ ]\)\ *\(\.\)\ *\(png\)\ *)_!\1\2\3\4)_g


#Deal with all lines

{:a;N;$!ba;s_\n_//_g;}

#Reopen root graph after graph closing
#s_}_\n&¤graph @g:"Root" {_g

#Transform Md titles into graph openings
#s_\#\+\([^/]*\)//_\n} \ngraph @g:"\1" {_g

#Remove temporary indications of newline
#s_//_ _g

#Surround RDF punctuation with spaces to prevent inclusion into pseudo-URI
s_[\.\;\,]_ & _g


#Dealing with [@§]

#s_\([@§]\)\ *_\n\1_g

#Dealing with namespace
#s_\([@§][^:^ ]\)*\ *\:_\1:_g


#################################################################################

#Tree branching management extension

#with namespace
#s_\(¥\)\ *\([^:^ ]*\)\ *\:\ *\(\"[^"]*\"\)_@\2:\3 . @\2:\3_g


################################################################################

#Alternative-label extension
#Concept: the author can write an alternate text that will be kept in the Md document or showed in the html+RDFa document, while the pseudo URI will be used in turtle and RDFa as the real URI


#Dealing with short label after namespace
s_\([@§][^ ^:]*\:\)\ *\([^ ^"]\+\)\ *\"\([^"^@^§^%^,^;^.]*\)\"_\3_g

#Dealing with long label after namespace
s_\([@§][^ ^:]*\:\)\ *\(\"[^"]*\"\)\([^"^@^§^%^,^;^.]*\)\"_\3_g

#Dealing with short label without namespace
s_\([@§]\)\ *\([^ ^:^"]\+\)\ *\"\([^"^@^§^%^,^;^.]*\)\"_\3_g

#Dealing with long label without namespace
s_\([@§]\)\(\"[^"]*\"\)\(\ *[^"^@^§^%^,^;^.]*\)\"_\3_g


#Dealing with punctuation
s_\,\ *\,\ *\"\([^"^,^;^.]*\)\"_ \1 _g
s_\;\ *\;\ *\"\([^"^,^;^.]*\)\"_ \1 _g
s_\.\ *\.\ *\"\([^"^,^;^.]*\)\"_ \1 _g

###############################################################################

#Dealing with short label after namespace
s_\([@§][^ ^:]*\:\)\ *\([^ ^"]\+\)_\2_g

#Dealing with long label after namespace
s_\([@§][^ ^:]*\:\)\ *\"\([^"]*\)\"_\2_g

#Dealing with short label without namespace
s_\([@§]\)\ *\([^ ^:^"]\+\)\(\ \)_\2\3_g

#Dealing with long label without namespace
s_\([@§]\)\"\([^"]*\)\"_\2_g

#Dealing with literals
s_%\ *\"\([^"]*\)\"_\1_g

#Removing spaces in labels
#{:b;s_\(@[^:]\+\:\"\)\ *\([^ ^"]\+\)\ \([^ ^"]*\)_\1\u\2\u\3_g;tb}
#{:c;s_\(§[^:]\+\:\"\)\ *\([^ ^"]\+\)\ \([^ ^"]*\)_\1\2\u\3_g;tc}

#Dealing with literals
#s_\(%\)\ *\(\"\)\ *\([^"]\+[^"^ ]\)\ *\(\"\)\ *\(%\)_\n\1\2""\3""\4\5\n\#_g

#Escape punctuation in literals (should be improved to handle multiple punctiation signs)
#{:d;s_\(%\"\"\"[^\.]*\)\.\([^\.]*\"\"\"%\)_\1\$PERIOD\2_g;td}
#{:e;s_\(%\"\"\"[^\;]*\)\;\([^\;]*\"\"\"%\)_\1\$SEMICOLON\2_g;te}
#{:f;s_\(%\"\"\"[^\,]*\)\,\([^\,]*\"\"\"%\)_\1\$COMA\2_g;tf}

#Dealing with punctuation
##Remove turtle only punctuation
s_\$\ *\.\ *\.__g
s_\$\ *\;\ *\;__g
s_\$\ *\,\ *\,__g

##Replace double punctuation by simple punctuation
s_\ *\.\ *\._\._g
s_\ *\,\ *\,_\,_g
s_\ *\;\ *\;_\;_g




#s_\([^$]\)\([\.\;\,{}]\)_\1\n\2\n\#_g
#s_\[\ *\]_\n&\n_g
#s_\[_\n&\n\#_g
#s_\]_\n&\n\#_g

#Correct the problem of punctuation in literals
#/\"\"\"/,/\"\"\"/{:h;N;$!bh;s_\.\n\#_\._g;th}

#Remove SMd markup
#s_[@§]\([^:]\+\:\)\"\ *\([^"]\+\)\ *\"_\1\2_g
#s_[@§]\([^:]\+\:\)\([^ ]\+\)\ _\1\2 _g
#s_%\"\"\"\([^"]\+\)\"\"\"%_"""\1"""_g
#s_\$\([.,;]\)_\1_g


#Clean begin and end of file
#s_^_\#_g
#s_}__1
#s_$_\n}_g


#Remove spaces around punctuation that have been added at the beginning of this script
s_\ \([\.\;\,]\)\ _\1_g



s_//_\n_g

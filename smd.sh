



function smd_to_trig {
echo "$(sed '1i \#Root\n' $1 |  awk '/\#/{x="F"++i;}{print > x".ttl";}'
#cat  ~/abc/smd/entete
for file in $(ls F*.ttl)
do  sed -f ~/abc/smd/prescript.sed -f ~/abc/smd/script.sed $file | sed '/^\#/d'
#echo 'r:Self sof:associatedNote """'
#sed -f ~/abc/smd/script-to-txt.sed $file
#echo '""" ; sof:originalNote """'
#cat $file
#echo '""" .}'
rm -f $file
done
cat ~/abc/smd/footer )" > $1.tmp.ttl
#print "output to "$1".tmp.ttl"
cat $1.tmp.ttl

}



function pass_query() {

#sed '1i \#Root\n' $1 |

check_file $1 | sed '{:a;N;$!ba;s_\n_ _g}' > insert_query.sparql
sed -i '/^\#/d' insert_query.sparql
cb u x insert_query.sparql


}

function query_from_text_input() {
read -e input
echo $input > tmp.smd
pass_query tmp.smd
rm tmp.smd
}

function check_file() {
echo "$(sed '1i \#Root\n' $1 |  awk '/\#/{x="F"++i;}{print > x".ttl";}'
cat  ~/abc/smd/entete
for file in $(ls F*.ttl)
do  sed -f ~/abc/smd/prescript.sed -f ~/abc/smd/script.sed $file | sed '/^\#/d'
echo 'r:Self sof:associatedNote """'
sed -f ~/abc/smd/script-to-txt.sed $file
echo '""" ; sof:originalNote """'
cat $file
echo '""" .}'
rm -f $file
done
cat ~/abc/smd/footer )" > $1.tmp.ttl 
#print "output to "$1".tmp.ttl"
cat $1.tmp.ttl

}


function check_from_text_input() {
read -e input
echo $input > tmp2.smd
check_file tmp2.smd
rm tmp2.smd

}


function pandoc_it() {
rm -f $1.html
awk '/\#/{x="F'$1'"++i;}{print > x".ttl";}' $1
for file in $(ls F$1*.ttl) ; do
sed -f ~/abc/smd/script-to-txt.sed $file > tmp.$file.md
echo "

![associated graph]("$file.png")" >> tmp.$file.md
pandoc tmp.$file.md >> $1.html 
rm -f $file tmp.$file.md
done

pandoc -s  $1.html -o $1.html
echo "output to" $1.html
}



function plot_graph_ttl_file () {
echo "from a turtle file"

awk '/\#/{x="F'$1'"++i;}{print > x".ttl";}' $1
for file in $(ls F$1*.ttl) ; do
rapper -i turtle $file -o dot | dot -Tsvg -Kfdp -LC10000 -o $file.svg
echo "you may want to change the algorithm used by graphviz"
gpicview $file.svg &
rm -f $file
done


}

function plot_graph_smd_file () {
 #"from a smd file, removing titles, hence all triples are in one graph
echo "the algorithm must be indicated in second argument
"
algo=$2

echo $algo

awk '/\#/{x="F'$1'"++i;}{print > x".ttl";}' $1


for file in $(ls F$1*.ttl) ; do
cat ~/abc/smd/prefixes.ttl > tmp.$file.ttl
sed '/^\ *\#/d; /^$/d ' $file | sed '1s_^[^@]*@_\n@_' | sed -f ~/abc/smd/prescript.sed | sed -f ~/abc/smd/script.sed >> tmp.$file.ttl

rapper -i turtle tmp.$file.ttl -o dot | dot -Tpng -K$algo -LC10000 -o $file.png
#gpicview $file.png &
rm -f $file tmp.$file.ttl

done
}

function plot_graph_text () {
echo "not yet implemented, use plot_graph_file instead"
}



function full () {
pandoc_it $1
plot_graph_smd_file $1 $2
#firefox $1.html &
}

function help() {
echo "
smd 
- file <file>
- text ; then iput SMd text
- pandoc
- check_text
- check_file
- plot_graph_file <file> 
- plot_graph_text ; then input SMd text.
- full <file> : extract a turtle file + a html file + a png file
"
}

case "$1" in
file ) shift 
       pass_query $1
       ;;
text ) query_from_text_input
	;;
pandoc ) shift
       pandoc_it $1
	;;
check_file ) shift
        check_file $1
        ;;
check_text ) shift
        check_from_text_input 
	;;
plot_graph_ttl_file ) shift
           plot_graph_ttl_file $1
        ;;
plot_graph_smd_file ) shift
            plot_graph_smd_file $1 $2
       ;;
plot_graph_text ) shift
           plot_graph_text $1
        ;;  
smd_to_trig ) shift
           smd_to_trig $1
	;;
full ) shift
       full $1 $2
        ;;
*)  shift
         help
        ;;
esac


function smd() {
source ~/abc/smd/smd.sh
}

export -f smd

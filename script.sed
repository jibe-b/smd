
#Deal with all lines

{:a;N;$!ba;s_\n_//_g;}

#Reopen root graph after graph closing
#s_}_\n&\ngraph @g:"Root" {_g

#Transform Md titles into graph openings
s_\#\+\([^/]*\)//_\n} \ngraph @g:"\1" {_g

#Remove temporary indications of newline
s_//_ _g


#Surround RDF punctuation with spaces to prevent inclusion into pseudo-URI
#Note: RDF punctuation is the doubling of turtle punctuation
s_\.\ *\._ \.\. _g
s_\;\ *\;_ \;\; _g
s_\,\ *\,_ \,\, _g

#Dealing with [@&]

s_\([@&]\)\ *_\n\1_g

#Dealing with namespace
s_\([@&][^:^ ]\)*\ *\:_\1:_g



#Removing spaces in labels
##with namespace
{:b;s_\(@[^:]\+\:\ *\"\)\ *\([^ ^"]\+\)\ \([^ ^"]*\)_\1\u\2\u\3_g;tb}
{:c;s_\(&[^:]\+\:\ *\"\)\ *\([^ ^"]\+\)\ \([^ ^"]*\)_\1\2\u\3_g;tc}
#without namespace
{:d;s_\(@\ *\"\)\ *\([^ ^"]\+\)\ \([^ ^"]*\)_\1\u\2\u\3_g;td}
{:e;s_\(&\ *\"\)\ *\([^ ^"]\+\)\ \([^ ^"]*\)_\1\2\u\3_g;te}

#################################################################################

#Non complete triple extension
## £something

#Dealing with short label after namespace
s_\(£\)\([^ ^:]*\:\)\ *\([^ ^"]\+\)_\n@\2\3 &sof:related @owl:Thing ..\n\#_g

#Dealing with long label after namespace
s_\(£\)\([^ ^:]*\:\)\ *\"\([^"]*\)\"_\n@\2"\3" &sof:related @owl:Thing ..\n\#_g

#Dealing with short label without namespace
s_\(£\)\ *\([^ ^:^"]\+\)\(\ \)_\n@r:"\2" &sof:related @owl:Thing ..\n\#\3_g
#s_\(&\)\ *\([^ ^:^"]\+\)\(\ \)_\1sof:"\2"\n\#\3_g

#Dealing with long label without namespace
s_\(£\)\"\([^"]*\)\"_\n@r:"\2" &sof:related @owl:Thing ..\n\#_g
#s_\(&\)\"\([^"]*\)\"_\1sof:\2\n\#_g




##Singleton
###Single subject or object
#s_\(\.[^.]*@[^:]*\:[^ ]\+\ \)[^.]*\._\1 &sof:related @owl:Thing._g
#s_\(\.[^.]*@\ *[^ ]\+\ [^.]\+\)\._\1 &sof:related @owl:Thing._g

###Single property
#######copier coller rapide
#s_\(\.[^\.]\+&[a-z]\+\)[^\.]*\._\1 &sof:related @owl:Thing._g

###Single literal

##Uncomplete triple
###Subject, pedicate, X
###Subject, X, predicate
###X, predicate, object



#################################################################################

# Nested Named Graph extension

#s_\(&[^@]*\)\({[^}]*}\)\([^}]*}\)_\1 @$incr .. \3 graph @$incr \2_
#test:
#graph @g:main {@this &is { a @sentence &like @another . } .. @this &goes @on .. }
#note:should be tested with higer order nesting (twice nested)



#################################################################################

# give a name to unnamed Named graphs
#note : already done a bit in the nested named graphs extension

#s_\(}[^{]*\)\({\)_\1 graph @$incre \2_
# note: has to be coupled with a post-sed script, that replaces this shell variable with a unique number, or be placed into the prescript


#################################################################################

#Tree branching management extension

#with namespace
s_\(¥\)\ *@*\ *\([^:^ ]*\)\ *\:\ *\(\"[^"]*\"\)_@\2:\3 .. @\2:\3_g


################################################################################

#Alternative-label extension
#Concept: the author can write an alternate text that will be kept in the Md document or showed in the html+RDFa document, while the pseudo URI will be used in turtle and RDFa as the real URI


#Dealing with short label after namespace
s_\([@&][^ ^:]*\:\)\ *\([^ ^"]\+\)\(\ *\"[^"^@^&^%]*\"\)_\1\2\n\#\3_g

#Dealing with long label after namespace
s_\([@&][^ ^:]*\:\)\ *\"\([^"]*\)\"\([^"^@^&^%]*\"\)_\1"\2"\n\#\3_g

#Dealing with short label without namespace
s_\(@\)\ *\([^ ^:^"]\+\)\ *\"\([^"^@^&^%]*\)\"_\1r:\2\n\#"\3"_g
s_\(&\)\ *\([^ ^:^"]\+\)\ *\"\([^"^@^&^%]*\)\"_\1sof:\"\2\"\n\#"\3"_g

#Dealing with long label without namespace
s_\(@\)\(\"[^"]*\"\)\(\ *[^"^@^&^%]*\)\"_\1r:\2\n\#\3_g
s_\(&\)\(\"[^"]*\"\)\(\ *[^"^@^&^%]*\)\"_\1sof:\2\n\#\3_g

#Dealing with punctuation
s_\,\ *\,\ *\(\"[^\"]*\"\)_\n\,\,\n\#\1_g
s_\;\ *\;\ *\(\"[^\"]*\"\)_\n\;\;\n\#\1_g
s_\.\ *\.\ *\(\"[^\"]*\"\)_\n\.\.\n\#\1_g


###############################################################################

#Dealing with short label after namespace
s_\([@&][^ ^:]*\:\)\ *\([^ ^"]\+\)_\1\2\n\#_g

#Dealing with long label after namespace
s_\([@&][^ ^:]*\:\)\ *\"\([^"]*\)\"_\1"\2"\n\#_g

#Dealing with short label without namespace
s_\(@\)\ *\([^ ^:^"]\+\)\(\ \)_\1r:"\2"\n\#\3_g
s_\(&\)\ *\([^ ^:^"]\+\)\(\ \)_\1sof:"\2"\n\#\3_g

#Dealing with long label without namespace
s_\(@\)\"\([^"]*\)\"_\1r:"\2"\n\#_g
s_\(&\)\"\([^"]*\)\"_\1sof:\2\n\#_g

#Dealing with literals
s_\(%\)\ *\(\"\+\)\ *\([^"]\+\)\ *\(\"\+\)\ *_\n\1\2""\3""\4\n\#_g

#Escape punctuation in literals (should be improved to handle multiple punctiation signs)
#{:d;s_\(%\"\"\"[^\.]*\)\.\([^\.]*\"\"\"%\)_\1\$PERIOD\2_g;td}
#{:e;s_\(%\"\"\"[^\;]*\)\;\([^\;]*\"\"\"%\)_\1\$SEMICOLON\2_g;te}
#{:f;s_\(%\"\"\"[^\,]*\)\,\([^\,]*\"\"\"%\)_\1\$COMA\2_g;tf}

#Dealing with punctuation
s_\.\._\n\.\n\#_g
s_\;\;_\n\;\n\#_g
s_\,\,_\n\,\n\#_g
s_}_\n}\n\#_g
s_{_\n{\n\#_g

# Dealing with blank nodes

s_\[\ *\]_\n&\n_g
#let's take care not to be in contradiction with []() of the Md syntax
#just remove them (temporarily)
s_\!*\ *\[[^]]*\]\ *([^)]*)__g
#then
s_\[_\n&\n\#_g
s_\]_\n&\n\#_g

#Correct the problem of punctuation in literals
#/\"\"\"/,/\"\"\"/{:h;N;$!bh;s_\.\n\#_\._g;th}

#Remove SMd markup
s_[@&]\([^:]\+\:\)\"\ *\([^"]\+\)\ *\"_\1\2_g
s_[@&]\([^:]\+\:\)\([^ ]\+\)\ _\1\2 _g
s_%\"\"\"\([^"]\+\)\"\"\"_"""\1"""_g
#s_\$\([.,;]\)_\1_g
#s_¤__g

#Clean begin and end of file
#1s_^_graph g:Root {\n\#_
1s_^_\#_
s_}__1
#s_$_\n}_g

#$s_}\ *$__

# SMd Tool

Converts SMd file to turtle and Md

## Featured command

> . smd.sh plot_graph_smd_file <file>

outputs a visualization of the graph contained in a SMd file.


## Dependencies

### Necessary

- [sed]()

### If not satisfied, limited function of the program

- [pandoc](pandoc.org)
- [blazegraph](blazegraph.com) (running at http://localhost:9999/bigdata/)
- [graphviz](graphviz.org)

## Commands

### Check the result

> smd check \<file.smd\>

> smd check_text

then input text (one line for the moment)

### Pass the insert query to the graph

**Blazegraph must be running**

> smd file \<file.smd\>

> smd text

then input the text (one line for the moment)

### Produce an html file from (based on pandoc)

**requires pandoc**

smd pandoc <file.smd>


## SMd to RDFa

Note: in alpha state. Parsing issues exist from SMd to RDFa.

### Commands

sed -f script-smd-to-rdfa.sed <file.smd>



## Credits

this program uses [sed](),
[pandoc](pandoc.org),
[blazegraph](blazegraph.com) 
and uses an extension of the [Markdown](http://daringfireball.com/) syntax.
